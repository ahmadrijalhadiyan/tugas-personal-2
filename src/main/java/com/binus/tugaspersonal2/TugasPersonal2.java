/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.binus.tugaspersonal2;
import java.io.IOException;
import java.util.Scanner;
/**
 *
 * @author rijal
 */
public class TugasPersonal2 {
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        boolean cek=true;
	while (cek) {	
		
		int input_angka, angkabeda=0, angkaarit = 1, angkageo=1, angkafaktorial=1;
		int minInput = 1;
		int maxInput = 10;
		int maxbeda = 10;
		boolean done=false;
		boolean done2=false;
                
                System.out.println("\n		Belajar Deret Aritmatika, Geometri dan Menghitung Faktorial		 ");
                System.out.println("		===========================================================		 ");

		do {
			System.out.print("Masukan banyak angka yang mau dicetak [2-10] : ");
                        while (!input.hasNextInt()) {
			System.out.println("\n Program hanya bisa menerima inputan bilangan bulat.\n");
			System.out.print("Masukan banyak angka yang mau dicetak [2-10] : ");
			input.next();
                        }
			input_angka = input.nextInt();
			if(input_angka < maxInput && input_angka > minInput)
                            {
				done=true;
                            }else {
                                System.out.println("\n"+"Inputan yang Anda Masukan Tidak Ada di Antara [2-10]."+"\n");
                        }
		}while(!done);
			
		do {
			System.out.print("Masukan beda masing - masing angka [2-9] : ");
                        while (!input.hasNextInt()) {
			System.out.println("\n Program hanya bisa menerima inputan bilangan bulat.\n");
			System.out.print("Masukan banyak angka yang mau dicetak [2-9] : ");
			input.next();
                        }
			angkabeda=input.nextInt();
			if (angkabeda < maxbeda && angkabeda > minInput) 
				{
					done2=true;
				}else {
                                System.out.println("\n"+"Inputan yang Anda Masukan Tidak Ada di Antara [2-9]."+"\n");
                        }
		}while(!done2);
		

			System.out.println("\n		Hasil Deret Aritmatika, Geometri dan Faktorial		 ");
			System.out.println("		==============================================		 ");
			System.out.println ("Deret Aritmatika = ");
		for (int i=0; i<input_angka; i++) 
            {
            	System.out.print(angkaarit + " ");
            	angkaarit += angkabeda;
            }
		
            	System.out.println ("\nDeret Geometri = ");
                for (int j=0; j<input_angka; j++) {
            		System.out.print(angkageo + " ");
            		angkageo *= angkabeda;
            	}
		
		System.out.println ("\nFaktorial dari " + input_angka + " = ");
		for (int k=input_angka; k>0; k--) {
			angkafaktorial *=k;
			if(k>1)
				System.out.print(k+" x ");
			else
				System.out.print(k);
		}
		System.out.println(" = " + angkafaktorial);
		   
		System.out.print("\nIngin coba lagi (Y/N) ? ");
                Scanner x = new java.util.Scanner(System.in);
                String coba = x.nextLine();
                if (coba.equalsIgnoreCase("N"))
                    cek = false;
                else if (coba.equalsIgnoreCase("Y"))
                    cek = true;
        else
            System.exit(0);
     
		}
	}
}

